import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor() {
    var config = {
      apiKey: "AIzaSyCPy9EUtfaiHGd3Kq05VP_-PSi3PZhxRcY",
      authDomain: "book-app-65511.firebaseapp.com",
      databaseURL: "https://book-app-65511.firebaseio.com",
      projectId: "book-app-65511",
      storageBucket: "",
      messagingSenderId: "88563249828"
    };
    firebase.initializeApp(config);
  }
}
